package com.example.demo;

import com.example.demodependency.DownstreamThing;
import lombok.Data;

@Data
public class Thing {

    private DownstreamThing dThing;
    private String otherString;

    public Thing() {
    }

    public Thing(DownstreamThing dThing, String otherString) {
        this.dThing = dThing;
        this.otherString = otherString;
    }
}
