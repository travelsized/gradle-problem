package com.example.demo;

import com.example.demodependency.DownstreamThing;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ThingController {

    @GetMapping("")
    @ResponseBody
    public Thing getThing() {
        return new Thing(new DownstreamThing(1L, "Downstream"), "other");
    }
}
