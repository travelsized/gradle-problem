package com.example.demodependency;

import lombok.Data;

@Data
public class DownstreamThing {

    private Long id;
    private String description;

    public DownstreamThing() {
    }

    public DownstreamThing(Long id, String description) {
        this.id = id;
        this.description = description;
    }
}
